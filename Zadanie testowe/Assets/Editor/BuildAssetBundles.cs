using UnityEngine;
using UnityEditor;
using System.IO;

public class BuildAssetBundles : Editor
{
    [MenuItem("AssetBundle/Build Asset Bundles")]
    static void BuildObjects()
    {

        var path = Application.dataPath + "/Input";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows);
    }
}