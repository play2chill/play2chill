using UnityEngine;

public class LoadObjects : MonoBehaviour
{
    #region private variable
    private AssetBundle bundle;
    private UIController controller;
    #endregion
    #region public variable
    public string[] objectName;
    public Object spawnedObject;
    #endregion

    private void Start()
    {
        controller = FindObjectOfType<UIController>();
        LoadAssetBundle();
        InstantiateObject(0);
    }
    private void LoadAssetBundle()
    {
        bundle = AssetBundle.LoadFromFile(Application.dataPath + "/Input/models");
        objectName = bundle.GetAllAssetNames();
    }

    public void InstantiateObject(int objectNumber)
    {
        if (spawnedObject != null)
        {
            DestroyImmediate(spawnedObject);
            Camera.main.transform.position = new Vector3(0f, 0f, -10f);
            controller.scale = -10f;
        }
        var objToInstantiate = bundle.LoadAsset(objectName[objectNumber]);
        spawnedObject = Instantiate(objToInstantiate);
    }
}
