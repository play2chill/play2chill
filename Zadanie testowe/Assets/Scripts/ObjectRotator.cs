using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    #region private variable
    private float sensitivity;
    private Vector3 mouseReference;
    private Vector3 mouseOffset;
    private Vector3 rotation;
    private bool isRotating;
    private UIController uiController;
    #endregion

    private void Start()
    {
        sensitivity = 0.4f;
        rotation = Vector3.zero;
        uiController = FindObjectOfType<UIController>();
    }

    private void Update()
    {
        if (isRotating)
        {
            mouseOffset = (Input.mousePosition - mouseReference);
            if (uiController.rotateY)
                rotation.y = -(mouseOffset.x) * sensitivity;
            if (uiController.rotateX)
                rotation.x = -(mouseOffset.y) * sensitivity;
            transform.eulerAngles += rotation;
            mouseReference = Input.mousePosition;
        }
    }

    public void OnMouseDown()
    {
        if (uiController.rotateY || uiController.rotateX)
        {
            isRotating = true;
            mouseReference = Input.mousePosition;
        }
    }

    public void OnMouseUp()
    {
        isRotating = false;
    }
}