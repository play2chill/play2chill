using System.Collections;
using System.IO;
using UnityEngine;

public class ScreanShotMaker : MonoBehaviour
{
    #region private variable
    private int FileCounter = 0;
    private Camera Cam;
    private WaitForEndOfFrame frameEnd = new WaitForEndOfFrame();
    #endregion

    public void CamCapture()
    {
        if (Cam == null)
            Cam = GetComponent<Camera>();
        StartCoroutine(TakeSnapshot(Cam.pixelWidth, Cam.pixelHeight));
    }

    private IEnumerator TakeSnapshot(int width, int height)
    {
        yield return frameEnd;
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = Cam.targetTexture;
        Cam.Render();
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGB24, true);
        texture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        texture.LoadRawTextureData(texture.GetRawTextureData());
        texture.Apply();
        RenderTexture.active = currentRT;
        var Bytes = texture.EncodeToPNG();
        Destroy(texture);
        var path = Application.dataPath + "/Output/";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        File.WriteAllBytes(path + System.DateTime.Now.ToString("HH-mm-ss") + "-" + FileCounter + ".png", Bytes);
        FileCounter++;
    }
}
