using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region private variable
    private LoadObjects loadObjects;
    private float number = 0;
    private bool zoom = false;
    private int zoomValue;
    #endregion
    #region public variable
    public float scale = -10f;
    public bool rotateX = true;
    public bool rotateY = true;
    #endregion
    private void Start()
    {
        loadObjects = FindObjectOfType<LoadObjects>();
    }

    #region Spawn Option
    public void SpawnNext()
    {
        number++;
        number = Modulo(number, loadObjects.objectName.Length);
        loadObjects.InstantiateObject((int)number);
    }

    public void SpawnPrevious()
    {
        number--;
        number = Modulo(number, loadObjects.objectName.Length);
        loadObjects.InstantiateObject((int)number);
    }

    public float Modulo(float a, float b)
    {
        return a - b * Mathf.Floor(a / b);
    }
    #endregion

    #region Zoom Option
    void Update()
    {
        if (zoom)
        {
            scale += zoomValue * Time.deltaTime;
            Camera.main.transform.position = new Vector3(0f, 0f, scale);
        }
    }

    public void Zoom(int value)
    {
        zoom = !zoom;
        zoomValue = value;
    }
    #endregion

    #region Rotate Option
    public void ChangeToggleY(Toggle toogle)
    {
        rotateY = toogle.isOn;
    }
    public void ChangeToggleX(Toggle toogle)
    {
        rotateX = toogle.isOn;
    }
    #endregion

    public void Quit()
    {
        Application.Quit();
    }
}
